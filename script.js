function sendCommand(command, ip, params, args) {

    var url = apiPath + command + "?ip=" + ip;

    for (let i = 0; i < args.length; i++) {
        url += "&" + params[i] + "=" + args[i];
    }

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            M.toast({html: JSON.parse(this.responseText) });
        }
    };

    console.log(url);
    xhr.open("GET", url, true);
    xhr.timeout = 2000;
    xhr.ontimeout = function () { M.toast({html: 'Request timed out'}); }
    xhr.send();
}

const MagicPickr = Pickr.create({
    el: '.color-picker',
    theme: 'nano',
    inline: true,
    default: '#ffffff',

    swatches: [
        'rgba(244, 67, 54, 1)',
        'rgba(233, 30, 99, 1)',
        'rgba(156, 39, 176, 1)',
        'rgba(103, 58, 183, 1)',
        'rgba(63, 81, 181, 1)',
        'rgba(33, 150, 243, 1)',
        'rgba(3, 169, 244, 1)',
        'rgba(0, 188, 212, 1)',
        'rgba(0, 150, 136, 1)',
        'rgba(76, 175, 80, 1)',
        'rgba(139, 195, 74, 1)',
        'rgba(205, 220, 57, 1)',
        'rgba(255, 235, 59, 1)',
        'rgba(255, 193, 7, 1)'
    ],

    components: {

        // Main components
        preview: true,
        opacity: false,
        hue: true,

        // Input / output Options
        interaction: {
            hex: false,
            rgba: false,
            hsla: false,
            hsva: false,
            cmyk: false,
            input: false,
            clear: true,
            save: true
        }
    }
}).on('save', () => {
    isMagicColorEnabled = true;
    updateMagic();
});

let isMagicColorEnabled = false;

function updateMagic(setMagic = false) {
    color = MagicPickr.getColor();

    if (setMagic == true) isMagicColorEnabled = false;

    if (isMagicColorEnabled == true) {
        sendCommand('action/magic/color', '192.168.1.91', ['r', 'g', 'b'], rgbaToRgb(color.toRGBA(), MagicBrightness.value/100));
        sendCommand('action/magic/color', '192.168.1.92', ['r', 'g', 'b'], rgbaToRgb(color.toRGBA(), MagicBrightness.value/100));
        sendCommand('action/magic/color', '192.168.1.93', ['r', 'g', 'b'], rgbaToRgb(color.toRGBA(), MagicBrightness.value/100));
    } else {
        sendCommand('action/magic/white', '192.168.1.91', ['brightness'], [parseInt(MagicBrightness.value * 2.55)]);
        sendCommand('action/magic/white', '192.168.1.92', ['brightness'], [parseInt(MagicBrightness.value * 2.55)]);
        sendCommand('action/magic/white', '192.168.1.93', ['brightness'], [parseInt(MagicBrightness.value * 2.55)]);
    }
}

function toggleMagic(state) {
    sendCommand('action/magic/power', '192.168.1.91', ['power'], [(state == true) ? 1 : 0]);
    sendCommand('action/magic/power', '192.168.1.92', ['power'], [(state == true) ? 1 : 0]);
    sendCommand('action/magic/power', '192.168.1.93', ['power'], [(state == true) ? 1 : 0]);
}

function rgbaToRgb(colors, brightness) {
    return [parseInt(colors[0] * brightness), parseInt(colors[1] * brightness), parseInt(colors[2] * brightness)];
}